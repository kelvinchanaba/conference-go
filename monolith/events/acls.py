import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Use the Pexels API
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


# def get_weather_data(city, state):
#     # Use the Open Weather API
#     url = "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}"
#     response = requests.get(url, headers=headers, params=params)
#     content = json.loads(response.content)

#         headers = {"Authorization": OPEN_WEATHER_API_KEY}
#     params = {
#         "lat": 1,
#         "lon": city + " " + state
#     }
#     response = requests.get(url, headers=headers, params=params)
#     content = json.loads(response.content)
#     try:
#         return {"picture_url": content["photos"][0]["src"]["original"]}
#     except:
#         return {"picture_url": None}
#     pass
